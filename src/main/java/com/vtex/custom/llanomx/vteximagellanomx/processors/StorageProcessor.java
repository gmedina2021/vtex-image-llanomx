package com.vtex.custom.llanomx.vteximagellanomx.processors;

import java.io.File;

import com.vtex.custom.llanomx.vteximagellanomx.util.*;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;

public class StorageProcessor implements Processor {

    private String accesskey;
    private String secretkey;
    private String region;
    private String bucketname;

    public StorageProcessor(String accesskey, String secretkey, String region, String bucketname) {
        this.accesskey = accesskey;
        this.secretkey = secretkey;
        this.region = region;
        this.bucketname = bucketname;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn();
        String SKU = message.getHeader("SKU", String.class);
        String filename = message.getHeader("CamelFileName", String.class);
        String path = message.getHeader("CamelFileLocalWorkPath", String.class);
        File file = new File( path );

        BasicAWSCredentials credentials = new BasicAWSCredentials(accesskey, secretkey);
        AmazonS3 s3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.fromName(region)).build();

        PutObjectRequest request = new PutObjectRequest(bucketname, SKU + "/" + filename, file)
                .withCannedAcl(CannedAccessControlList.PublicRead);
        
        s3.putObject(request);

        String url = s3.getUrl(bucketname, SKU + "/" + filename).toString();
        String url1 = url.replace(Constants.URL_TARGET,
                Constants.URL_S3 + bucketname + "/");

        message.setHeader("url", url1);
    }

}
