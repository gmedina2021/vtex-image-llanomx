package com.vtex.custom.llanomx.vteximagellanomx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VtexImageLlanomxApplication {

	public static void main(String[] args) {
		SpringApplication.run(VtexImageLlanomxApplication.class, args);
	}

}
