package com.vtex.custom.llanomx.vteximagellanomx.processors;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import com.vtex.custom.llanomx.vteximagellanomx.interfaces.FileNameDefinition;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class FileNameProcessor implements Processor, FileNameDefinition{
    private static final Log LOG = LogFactory.getLog( FileNameProcessor.class );
    private Map<String, String> contentTypes = null;

    public FileNameProcessor() {
		LOG.trace( "initializing instance" );
		contentTypes = new HashMap<>();
		contentTypes.put( "jpg", "image/jpeg" );
		contentTypes.put( "jpeg", "image/jpeg" );
		contentTypes.put( "tif", "image/tiff" );
		contentTypes.put( "tiff", "image/tiff" );
		contentTypes.put( "png", "image/png" );
		contentTypes.put( "mp4", "video/mp4" );
		contentTypes.put( "mov", "video/quicktime" );
	}

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn();
        message.setHeader("CamelFileAbsolute", true);
        String filename = message.getHeader(Exchange.FILE_NAME_ONLY, String.class);

        filename = filename.replaceAll("\\s", "").replaceAll("\\_+", "_").toUpperCase();

        Matcher matcher = SKU_PATTERN.matcher( filename );

        matcher.find();

        if ( matcher.matches() == false ) {
            throw new Exception("Wrong file name " + filename);
        } else {
            String sku = matcher.group(1);
            String seq = matcher.group(4);
            String ext = matcher.group(5);
    
            if( seq == null ) seq = "99";

            StringBuilder builder = new StringBuilder( sku );
            builder.append("_").append( Integer.parseInt(seq)).append(".").append(ext);
            message.setHeader("CamelFileName", builder.toString());
            message.setHeader("SKU", sku);
            message.setHeader("SEQ", seq);
        }
    }
}
