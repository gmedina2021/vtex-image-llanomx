package com.vtex.custom.llanomx.vteximagellanomx.processors;

import com.vtex.custom.llanomx.vteximagellanomx.pojos.SkuFilePojo;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;

public class VtexProcessor implements Processor {
    String baseUrl;
    String vtexappkey;
    String vtexapptoken;
    String uriskufile;

    public VtexProcessor(String baseUrl, String vtexappkey, String vtexapptoken, String uriskufile) {
        this.baseUrl = baseUrl;
        this.vtexappkey = vtexappkey;
        this.vtexapptoken = vtexapptoken;
        this.uriskufile = uriskufile;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        Message message = exchange.getIn();
        String filename = message.getHeader(Exchange.FILE_NAME_ONLY, String.class);
        String sku = message.getHeader("SKU", String.class);
        String urlfile = message.getHeader("url", String.class);
        Integer seq = message.getHeader("SEQ", Integer.class);

        Boolean IsMain = false;

        if ( seq == 1 ) {
            IsMain = true;
        }

        SkuFilePojo skuFile = new SkuFilePojo(IsMain, sku, filename, urlfile);

        WebClient client = WebClient.builder()
            .baseUrl(baseUrl)
            .defaultHeader("X-VTEX-API-AppKey", vtexappkey)
            .defaultHeader("X-VTEX-API-AppToken", vtexapptoken)
            .build();

        try {
            client.post().uri(uriskufile + sku + "/file")
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .bodyValue(skuFile)
                .retrieve()
                .bodyToMono(Void.class)
                .block();
        } catch (WebClientException e) {
            System.out.println(e.getMessage());
        }
    }

}
