package com.vtex.custom.llanomx.vteximagellanomx.routes;

import com.vtex.custom.llanomx.vteximagellanomx.processors.FileNameProcessor;
import com.vtex.custom.llanomx.vteximagellanomx.processors.StorageProcessor;
import com.vtex.custom.llanomx.vteximagellanomx.processors.VtexProcessor;

import org.apache.camel.CamelContext;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ImageRoute extends RouteBuilder {
    CamelContext context = new DefaultCamelContext();

    @Value("${ftp.media.endpoint}")
    private String ftpserver;

    @Value("${aws.accessKey}")
    private String accesskey;

    @Value("${aws.secretKey}")
    private String secretkey;

    @Value("${aws.region}")
    private String region;

    @Value("${aws.s3.bucketName}")
    private String bucketname;

    @Value("${vtex.appkey}")
    private String vtexappkey;

    @Value("${vtex.apptoken}")
    private String vtexapptoken;

    @Value("${vtex.baseurl}")
    private String baseurl;

    @Value("${vtex.uriskufile}")
    private String uriskufile;
    
    @Override
    public void configure() throws Exception {
        try {
            onException(Exception.class)
            .handled(true)
            .maximumRedeliveries(1)
            .redeliveryDelay(1000)
            .retryAttemptedLogLevel(LoggingLevel.INFO)
            .retriesExhaustedLogLevel(LoggingLevel.INFO);

            from(ftpserver)
            .log("Iniciando procesadores")
            .process(new FileNameProcessor())
            .process( new StorageProcessor(accesskey, secretkey, region, bucketname))
            .process( new VtexProcessor(baseurl, vtexappkey, vtexapptoken, uriskufile))
            .to("mock:result");
        } catch( Exception e ) {
            e.printStackTrace();
        }
    }
    
}
