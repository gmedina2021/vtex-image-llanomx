package com.vtex.custom.llanomx.vteximagellanomx.util;

public class Constants {
    public static final String URL_TARGET = "https://llanomx.wcaas.media.s3.amazonaws.com/";
    public static final String URL_S3 = "https://s3.amazonaws.com/";
}
