package com.vtex.custom.llanomx.vteximagellanomx.interfaces;

import java.util.regex.Pattern;

public interface FileNameDefinition {
    public static final Pattern SKU_FILTER = Pattern.compile( "^(\\d+\\.\\d+)((\\_|\\-)(\\d{1,2}))?\\.(jpg|jpeg|tif|tiff|png)$", Pattern.CASE_INSENSITIVE );
	public static final Pattern SKU_PATTERN = Pattern.compile( "^(\\d+\\.\\d+)((\\_|\\-)(\\d{1,2}))?\\.(jpg|jpeg|tif|tiff|png)$", Pattern.CASE_INSENSITIVE );
}
