package com.vtex.custom.llanomx.vteximagellanomx.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SkuFilePojo {
    @JsonProperty("IsMain")
    private Boolean IsMain;

    @JsonProperty("Label")
    private String Label;

    @JsonProperty("Name")
    private String Name;

    @JsonProperty("Url")
    private String Url;

    public SkuFilePojo(Boolean IsMain, String Label, String Name, String Url) {
        this.IsMain = IsMain;
        this.Label = Label;
        this.Name = Name;
        this.Url = Url;
    }
}
